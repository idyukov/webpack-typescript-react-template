const { resolve: pathResolve } = require('path');

const { DefinePlugin } = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

const package = require('./package.json');

const sourcePath = pathResolve(__dirname, './src');
const outputPath = pathResolve(__dirname, './build');

module.exports = (env, argv) => {
  const isProduction = argv.mode === 'production';

  const config = require(`./config/${argv.mode}`);

  return {
    entry: {
      main: [package.main],
    },
    output: {
      filename: 'index.js',
      chunkFilename: 'routes/[chunkhash].js',
      path: outputPath,
    },
    target: 'web',
    devtool: 'source-map',
    plugins: [
      ...(isProduction ? [new CleanWebpackPlugin()] : []),
      new DefinePlugin({
        VERSION: package.version,
        RUNTIME: argv.mode,
        CONFIG: JSON.stringify(config),
      }),
      new ESLintPlugin(),
      new HtmlWebpackPlugin({
        config,
        base: config.basePath ?? '/',
        template: pathResolve(sourcePath, 'index.html'),
      }),
      new MiniCssExtractPlugin({
        filename: 'styles/[chunkhash].css',
      }),
      new CopyWebpackPlugin({
        patterns: [
          {
            from: pathResolve(sourcePath, 'assets'),
            to: pathResolve(outputPath, 'assets'),
          },
        ],
      }),
    ],
    ...(isProduction && {
      optimization: {
        minimize: true,
        minimizer: [new TerserPlugin(), new CssMinimizerPlugin()],
      },
    }),
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          loader: 'ts-loader',
          exclude: '/node_modules/',
          options: {
            compilerOptions: {
              declaration: false,
              declarationMap: false,
            },
          },
        },
        {
          test: /\.(sa|sc|c)ss$/i,
          use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            {
              loader: 'sass-loader',
              options: {
                implementation: require('node-sass'),
              },
            },
            'postcss-loader',
          ],
        },
      ],
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js'],
      plugins: [new TsconfigPathsPlugin({})],
    },
    devServer: {
      // hot: true,
      // injectHot: true,
      liveReload: true,
      open: true,
      stats: 'normal',
      contentBase: './src/assets',
      historyApiFallback: true,
    },
  };
};
