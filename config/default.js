const pkg = require('../package.json');

module.exports = {
  app: {
    name: pkg.name, // 'React App Template',
    version: pkg.version,
  },
};
