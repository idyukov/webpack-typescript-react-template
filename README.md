##### Intro

- Webpack
- TypeScript
- React and react-based state management + custom hooks (async effects and thunk actions)
- Code splitting (separate chunk per route module)
- Config injection for development / production environments (see `configs` dir)
- SCSS styling
- ...

##### Start development server:

```
yarn start
```

##### Build production version:

```
yarn build
```
