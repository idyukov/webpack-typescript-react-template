import React from 'react';

import { useAppReducer, useAsyncEffectOnce, useThunkEffect } from '@app/core';
import { updateTodoList, toggleTodoItem } from '@app/actions/todos';
import { AsyncDestructor, AsyncEffectCallback, ThunkEffectCallbackWithParams } from '@app/types/core';
import { fetchJson } from '@app/utils';

import { TodoList, ToggleTodoItemPayload } from './types';
import View from './View';

const TODOS_URL = 'https://jsonplaceholder.typicode.com/todos';

const fetchTodoList: AsyncEffectCallback = async ({ dispatch }): Promise<AsyncDestructor> => {
  const items = await fetchJson<TodoList>(TODOS_URL);
  dispatch(updateTodoList(items));
  // unmount
  return () => {
    // eslint-disable-next-line no-console
    console.log('UNMOUNT');
  };
};

const doToggleTodoItem: ThunkEffectCallbackWithParams<ToggleTodoItemPayload> = async (
  { id, newState },
  { dispatch }
) => {
  dispatch(toggleTodoItem({ id, newState: undefined }));
  const response = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`, {
    method: 'PATCH',
    body: JSON.stringify({
      completed: newState,
    }),
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    },
  });
  if (response.ok) {
    dispatch(toggleTodoItem({ id, newState }));
  }
};

const TodosRoute: React.FunctionComponent = () => {
  const [state] = useAppReducer();

  useAsyncEffectOnce(fetchTodoList);

  return React.createElement(View, {
    todos: state.todos as TodoList,
    onToggleItem: useThunkEffect(doToggleTodoItem),
  });
  // TSX:
  // return <View todos={state.todos as TodoList} onToggleItem={useThunkEffect(doToggleTodoItem)} />
};

export { TodosRoute as default, View };
