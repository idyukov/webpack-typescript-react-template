export type TodoItem = {
  id: number;
  userId: number;
  title: string;
  completed: boolean | undefined;
};

export type TodoList = Array<TodoItem>;

export type ToggleTodoItemPayload = {
  id: number;
  newState: boolean | undefined;
};
