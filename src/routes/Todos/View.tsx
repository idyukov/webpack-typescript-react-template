import React from 'react';

import Spinner from '@app/pieces/Spinner';

import { TodoItem, TodoList, ToggleTodoItemPayload } from './types';

import './styles.scss';

export type TodosProps = {
  todos?: TodoList;
  onToggleItem: (args: ToggleTodoItemPayload) => void;
};

const View: React.FunctionComponent<TodosProps> = ({ todos, onToggleItem }: TodosProps) => (
  <>
    {todos ? (
      todos.map((item: TodoItem) => (
        <div key={item.id} className="form-group">
          <label className="form-checkbox" htmlFor={`checkbox-${item.id}`}>
            <input
              type="checkbox"
              id={`checkbox-${item.id}`}
              checked={item.completed === true}
              onChange={() => onToggleItem({ id: item.id, newState: !item.completed })}
            />
            <i className={`form-icon${item.completed === undefined ? ' loading no-border' : ''}`} />
            {item.title}
          </label>
        </div>
      ))
    ) : (
      <Spinner />
    )}
  </>
);

export default View;
