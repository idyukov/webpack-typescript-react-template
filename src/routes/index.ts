import React from 'react';

import { AppRouteSpec } from '@app/types';
import { base64Encode } from '@app/utils';

const routes: Array<AppRouteSpec> = [
  {
    name: 'Home',
    path: '/',
    view: React.lazy(() => import('./Home')),
    exact: true,
  },
  {
    name: 'About',
    path: '/about',
    view: React.lazy(() => import('./About')),
  },
  {
    name: 'Todos',
    path: '/todos',
    view: React.lazy(() => import('./Todos')),
  },
]
  // auto-generate menu item identitfiers
  .map((item) => (item.hasOwnProperty('id') ? item : Object.assign(item, { id: base64Encode(item.name) })));

export default routes;
