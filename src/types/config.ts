export type AppConfig = {
  app: {
    name: string;
    version: string;
  };
  // ...
};
