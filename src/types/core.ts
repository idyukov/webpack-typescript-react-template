import React, { Dispatch } from 'react';
import { AppConfig } from './config';
import { AppState as AppStateSpec } from './state';

export { AppConfig };

export class AppError extends Error {
  readonly errCode: string;

  readonly details: unknown;

  constructor(errCode: string, details?: unknown) {
    super(errCode);
    this.name = 'AppError';
    this.errCode = errCode;
    this.details = details;
  }
}

export type AnyPayload = any; // eslint-disable-line @typescript-eslint/no-explicit-any

export type AppState = AppStateSpec & Record<string, AnyPayload>;

export type AppContext = {
  dispatch: AppDispatch;
  state: AppState;
};

export type Action<P = AnyPayload> = { type: string; payload?: P };

export type ActionCreator<P = AnyPayload> = (payload?: P) => Action;

export type ActionHandler<P = AnyPayload> = (state: AppState, payload?: P) => AppState;

export type AsyncDestructor = () => void | Promise<void>;
export type AsyncEffectCallback = (context: AppContext) => void | Promise<void> | Promise<AsyncDestructor>;

export type ThunkEffectCallback = (context: AppContext) => void | Promise<void>;
export type ThunkEffectCallbackWithParams<P> = (params: P, context: AppContext) => void | Promise<void>;

export type AppDispatch<P = AnyPayload> = Dispatch<P>;

export type AppRouteSpec = {
  name: string;
  path: string;
  view: ReturnType<typeof React.lazy> | React.ComponentType<unknown>;
  exact?: boolean;
  id?: string;
} & Record<string, unknown>;
