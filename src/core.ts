import React, {
  Context,
  ConsumerProps,
  DependencyList,
  ProviderProps,
  ReactElement,
  Reducer,
  createContext,
  createElement,
  useContext,
  useEffect,
  useReducer,
  useRef,
} from 'react';

import {
  ERR_APP_BOOTSTRAP_IRRELEVANCE,
  ERR_INVALID_APP_CONTEXT_USAGE,
  ERR_UNDEFINED_ACTION_DISPATCH,
} from '@app/consts';
import { Action, ActionHandler, AsyncDestructor, AsyncEffectCallback } from '@app/types';
import {
  AppContext as AppContextSpec,
  AppState,
  ThunkEffectCallback,
  ThunkEffectCallbackWithParams,
} from '@app/types/core';
import { throwAppError } from '@app/utils';

const actions = new Map<string, ActionHandler>();

const initialState: AppState = {};

// application level reducer
const appReducer: Reducer<AppState, Action> = (state: AppState, action: Action): AppState | never =>
  (actions.get(action.type) || throwAppError(ERR_UNDEFINED_ACTION_DISPATCH, { action }))(state, action.payload);

// application context component
let appContext: AppContextSpec;
let AppContext: Context<AppContextSpec>;

export const AppContextProvider = (props: Omit<ProviderProps<AppContextSpec>, 'value'>): ReactElement | never =>
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
  appContext && AppContext
    ? createElement(AppContext.Provider, { ...props, value: appContext })
    : throwAppError(ERR_INVALID_APP_CONTEXT_USAGE);

export const AppContextConsumer = (props: ConsumerProps<AppContextSpec>): ReactElement =>
  AppContext ? createElement(AppContext.Consumer, props) : throwAppError(ERR_INVALID_APP_CONTEXT_USAGE);

export const useAppReducer = (): [AppState, React.Dispatch<Action>] => {
  const [state, dispatch] = useReducer(appReducer, initialState);
  Object.assign(appContext, { state, dispatch });
  return [state, dispatch];
};

export const useAppContext = (): AppContextSpec | never => {
  void useAppReducer();
  return useContext(AppContext) || throwAppError(ERR_INVALID_APP_CONTEXT_USAGE);
};

// application context hook
export const useThunkEffect = <P = void>(
  handler: ThunkEffectCallbackWithParams<P> | ThunkEffectCallback
): ((params: P) => void | Promise<void>) => {
  return (params: P) =>
    params
      ? (handler as ThunkEffectCallbackWithParams<P>)(params, appContext)
      : (handler as ThunkEffectCallback)(appContext);
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const useAsyncEffect = (effect: AsyncEffectCallback, deps?: DependencyList): void => {
  const destructorRef = useRef<void | AsyncDestructor>();
  useEffect(() => {
    const effectResult = effect(appContext);
    if (effectResult instanceof Promise) {
      // eslint-disable-next-line promise/always-return
      void effectResult.then((destructor: void | AsyncDestructor) => {
        destructorRef.current = destructor;
      });
      return () => void (destructorRef.current instanceof Function && destructorRef.current());
    }
    return effectResult;
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, react-hooks/exhaustive-deps
  }, deps);
};

const emptyDeps: DependencyList = [];
export const useAsyncEffectOnce = (effect: AsyncEffectCallback): void => useAsyncEffect(effect, emptyDeps);

// helper to define actions
export const defineAppActions = (spec: Record<string, ActionHandler>): void =>
  Object.entries(spec).forEach(([type, handler]) => actions.set(type, handler));

// extend application context
export const bootstrap = (entries: Omit<AppContextSpec, 'state' | 'dispatch'>): void => {
  AppContext
    ? throwAppError(ERR_APP_BOOTSTRAP_IRRELEVANCE)
    : (AppContext = createContext<AppContextSpec>(
        (appContext = Object.assign(Object.create(null), entries) as AppContextSpec)
      ));
};
