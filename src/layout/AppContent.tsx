import React, { Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';

import routes from '@app/routes';

import AppSpinner from '../pieces/Spinner';

const AppContent: React.FunctionComponent = () => (
  <main id="app-content">
    <Suspense fallback={<AppSpinner />}>
      <Switch>
        {routes.map((route) => (
          <Route key={route.id} path={route.path} exact={route.exact} component={route.view} />
        ))}
      </Switch>
    </Suspense>
  </main>
);

export default AppContent;
