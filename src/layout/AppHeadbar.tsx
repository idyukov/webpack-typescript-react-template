import React from 'react';

import config from '@app/config';

const AppHeadbar: React.FunctionComponent = () => {
  return (
    <header id="app-headbar">
      <img id="app-logo" src="assets/logo.svg" alt="app-logo" />
      <span id="app-name">{config.app.name}</span>
      <span id="app-version">&nbsp;v{config.app.version}</span>
    </header>
  );
};

export default AppHeadbar;
