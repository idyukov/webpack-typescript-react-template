import React from 'react';
import { BrowserRouter as AppRouter } from 'react-router-dom';

import '@app/styles';
import { AppContextProvider } from '@app/core';

import AppHeadbar from './AppHeadbar';
import AppSidebar from './AppSidebar';
import AppContent from './AppContent';

const AppLayout: React.FunctionComponent = () => {
  return (
    <AppContextProvider>
      <AppRouter>
        <AppHeadbar />
        <AppSidebar />
        <AppContent />
      </AppRouter>
    </AppContextProvider>
  );
};

export default AppLayout;
