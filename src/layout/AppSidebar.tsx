import React from 'react';
import { NavLink } from 'react-router-dom';

import routes from '@app/routes';

const AppSidebar: React.FunctionComponent = () => (
  <aside id="app-sidebar">
    <nav>
      <ul className="nav">
        {routes.map((route) => (
          <li key={route.id} className="nav-item">
            <NavLink to={route.path} exact={route.exact}>
              {route.name}
            </NavLink>
          </li>
        ))}
      </ul>
    </nav>
  </aside>
);

export default AppSidebar;
