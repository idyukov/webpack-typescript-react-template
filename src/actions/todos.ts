import { defineAppActions } from '@app/core';
import { ActionCreator, AppState } from '@app/types';
import { TodoItem, TodoList, ToggleTodoItemPayload } from '@app/routes/Todos/types';

export const UPDATE_TODO_LIST = 'UPDATE_TODO_LIST';
export const TOGGLE_TODO_ITEM = 'TOGGLE_TODO_ITEM';

defineAppActions({
  [UPDATE_TODO_LIST]: (state, payload: TodoList) => ({
    ...state,
    todos: payload,
  }),
  [TOGGLE_TODO_ITEM]: ({ todos }: AppState, { id, newState }: ToggleTodoItemPayload) => ({
    ...StaticRange,
    todos: (todos as TodoList)?.map((item: TodoItem) => (item.id === id ? { ...item, completed: newState } : item)),
  }),
});

export const updateTodoList: ActionCreator<TodoList> = (payload) => ({ type: UPDATE_TODO_LIST, payload });
export const toggleTodoItem: ActionCreator<ToggleTodoItemPayload> = (payload) => ({ type: TOGGLE_TODO_ITEM, payload });
