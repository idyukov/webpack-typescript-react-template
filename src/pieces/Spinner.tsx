import React from 'react';

const Spinner: React.FunctionComponent = () => (
  <div id="app-spinner">
    <i className="loading loading-lg" />
  </div>
);

export default Spinner;
