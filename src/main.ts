import React from 'react';
import ReactDOM from 'react-dom';

// import config from '@app/config';
import { bootstrap } from '@app/core';

import App from './layout';

// eslint-disable-next-line @typescript-eslint/require-await
const main = async () => {
  // bootstrap application and global context
  bootstrap({});
  ReactDOM.render(React.createElement(App), document.querySelector('#app-root'));
};

void main();
