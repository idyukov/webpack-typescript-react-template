import React from 'react';

import { Action, AppError } from '@app/types';
import { useAppContext } from '@app/core';

export const base64Decode = (str: string): string => decodeURIComponent(escape(window.atob(str)));
export const base64Encode = (str: string): string => btoa(unescape(encodeURIComponent(str)));

export const newAction = <P = undefined>(type: string, payload?: P): Action<P> => ({ type, payload });

export const throwAppError = (errCode: string, details?: unknown): never => {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-call
  throw new AppError(errCode, details);
};

export const fetchJson = async <R = Record<string, unknown> | Array<Record<string, unknown>>>(
  resource: RequestInfo,
  init?: RequestInit
): Promise<R> => fetch(resource, init).then<R>((response) => response.json());

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type AnyProps = Record<string, any>;
type MapProps = (
  context: ReturnType<typeof useAppContext> | ReturnType<typeof React.useContext>,
  ownProps: AnyProps
) => AnyProps;
type AnyComponent<P = AnyProps> = React.FunctionComponent<P>;

export const connect =
  (mapContextToProps?: MapProps | AnyProps) =>
  (Component: AnyComponent) =>
  // eslint-disable-next-line react/display-name
  (ownProps: AnyProps): ReturnType<typeof React.createElement> =>
    // prettier-ignore
    React.createElement(
      Component as React.FunctionComponent,
      {
        ...(
          mapContextToProps instanceof Function
            ? mapContextToProps(useAppContext(), ownProps)
            : mapContextToProps
        ),
        ...ownProps,
      }
    );
