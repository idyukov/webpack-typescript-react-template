import { AppConfig } from '@app/types';
/*
 * CONFIG global var is declared by WebPack during build process.
 * See webpack.config.js DefinePlugin section.
 */
declare const CONFIG: AppConfig;

export default CONFIG;
